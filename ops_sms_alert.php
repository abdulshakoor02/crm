<?php include_once("header.php");

$emp = $obj->display('dm_employee','id='.$_SESSION['ID']);
$emp1 = $emp->fetch_array();

$name = $emp1['name'];

?>
<div class="col-sm-10">
	<h4 class="mb-3" style="color:#2cb674;">Send Sms</h4>
	<div class="col-sm-10">
		
<form id= "sms" action="sms2.php" name="search" method="post">

<input type="hidden" name="emp" value="<?php echo $name; ?>">
 <div class="row">

 <div class="col-sm-2 form-group"><label>Type</label>
<select class="form-control" name="type" id="type" >
	<option value="">Select</option>
    <option value="Business">Business</option>
	<option value="Skill">Skill</option>
	<option value="Student">Student</option>
	<option value="Visit">Visit</option>
	<option value="Work">Work</option>
	</select>
</div>

<div class="col-sm-2 form-group"><label>Country</label>
<select class="form-control" name="country" id="country">
	<option value="">Select</option>
    <option value="2">Canada</option>
	<option value="1">Australia</option>
	</select>
</div>

<div class="col-sm-2 form-group"><label>&nbsp;</label><br />
<!-- modal start -->

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" id="buttn" data-toggle="modal" data-target="#exampleModal">
  Check No of clients
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Count of Clients</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- modal ends -->

</div>

 </div>


<div class="col-sm-12 form-group"><label>Message Body</label><textarea type="text" class="form-control" id="comments" name="comments"></textarea></div>	

<div class="col-sm-2 form-group"><label>&nbsp;</label><br /><input type="submit" class="btn btn-info" name="Send" value="Send" ></div>

</form>
 </div>
 </div>

 <script>
 $('#buttn').on('click',function(){
   var ty = $('#type').val();
   var coun = $('#country').val();
    $("#exampleModal").find('.modal-body').load('client_count.php?type='+ty+'&country='+coun,function(){
        // $('#tab18').modal({show:true});
    });
});
 </script>

 <?php include_once("footer.php"); ?>