<?php include_once("header.php");

?>
<div class="col-sm-10">
	<h4 class="mb-3" style="color:#2cb674;">Break down of leads per Branch</h4>
	<div id="alert_message"></div>
    <form name="search" action="" method="post">

    <div class="row">

<div class="col-sm-2 form-group">

<label >Start Date</label>

<input type="text" class="form-control" id="sdate" name="sdate" value="<?php if($_POST['sdate']) echo $_POST['sdate']; else  echo date('d-m-Y')?>" >

</div>

<div class="col-sm-2 form-group">

<label >End Date</label>

<input type="text" class="form-control" id="edate" name="edate" value="<?php if($_POST['edate']) echo $_POST['edate']; else echo date('d-m-Y')?>" >

</div>

<div class="col-sm-2 form-group"><label>Region</label>
<select class="form-control" name="region" id="region" >
	<option value="">Select</option>
	<?php $sou=$obj->display('dm_region','status=1 order by name');
	while($sou1=$sou->fetch_array())
	{
	?>
	<option value="<?php echo $sou1['id'];?>"  <?php if($sou1['id']==$_POST['region']) { echo 'selected="selected"';}?>><?php echo $sou1['name'];?></option>
	<?php } ?>
	</select>
</div>


<div class="col-sm-2 form-group"><label>&nbsp;</label><br /><input type="submit" class="btn btn-info" name="search" value="Search" ></div>

</div>

</form>

	<hr />
    <?php  if($_POST) { 
        if ($_POST['region'] !=""){
        $query= ' and region='.$_POST['region'];
        }
        ?>
	<table class="table table-striped table-bordered" id="myTable" style="width:100%">

				<thead>

					<tr>
						<th>Sr no.</th>
						<th>Branch</th>
						<th>Source</th>
                        <th>No of leads</th>
						</tr>
						</thead>
						<tbody>
							<?php
                            if ($_SESSION['TYPE']=="SA"){
                                $result = $obj->display3("SELECT COUNT(*) as total, (SELECT name from dm_source WHERE id=market_source) as source,(SELECT name from dm_region WHERE id=region) as branc FROM `dm_lead` WHERE regdate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."'".$query." GROUP by market_source");
                            }
                            else {
                            $result = $obj->display3("SELECT COUNT(*) as total, (SELECT name from dm_source WHERE id=market_source) as source,(SELECT name from dm_region WHERE id=region) as branc FROM `dm_lead` WHERE regdate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' and region=".$_SESSION['REGION']." GROUP by market_source");
                            }
							// print_r($result);die;
							if($result->num_rows>0)
							{
								$i=1;
								while($row=$result->fetch_assoc())
								{
                                    // $can=$obj->display3("SELECT COUNT(*) as can FROM `dm_lead` WHERE regdate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Skill' and country_interest=2 and region=".$row['region']);$can1=$can->fetch_assoc();
                                    // $aus=$obj->display3("SELECT COUNT(*) as aus FROM `dm_lead` WHERE regdate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Skill' and country_interest=1 and region=".$row['region']);$aus1=$aus->fetch_assoc();
                                    // $vis=$obj->display3("SELECT COUNT(*) as vis FROM `dm_lead` WHERE regdate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Visit' and region=".$row['region']);$vis1=$vis->fetch_assoc();
                                    // $stud=$obj->display3("SELECT COUNT(*) as stud FROM `dm_lead` WHERE regdate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Student' and region=".$row['region']);$stud1=$stud->fetch_assoc();
									?>
									<tr>
										<td><?=$i;?></td>
                                        <td><?=$row['branc'];?></td>
										<td><?=$row['source'];?></td>
										<td><?=$row['total'];?></td>
									</tr>
									<?php
									$i++;
								}
							}
							?>
						</tbody>	
						</table>
                        <?php } ?>
						</div>
						<?php include_once('footer.php');?>
						<script>
                        $(function(){
$('#sdate').datepicker({    format: 'dd-mm-yyyy',	autoclose: true}); 
$('#edate').datepicker({    format: 'dd-mm-yyyy',	autoclose: true}); 
});
							$(document).ready(function(){
								$('#myTable').DataTable({
                                    responsive:true,
                                    dom:'Bfprt',
                                    buttons: [
                                    {
                                        extend:'excel',
                                        title:'Leads Report',
                                        messageTop:'Leads Breakdown in terms of Source'
                                    }]
								});
								});
						</script>	
                        <script>
    $(document).ready(function(){
        $('#mydataTable').DataTable({
            responsive: true,
            dom:'Bfprt',
            buttons: [
            {
            	extend:'excel',
            	title:'Contracts Report',
            	messageTop:'Pending Contracts'
            }]
        });
    });
</script>		