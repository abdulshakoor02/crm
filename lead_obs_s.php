<?php include_once("include/config.php");	
  require_once("mpdf/autoload.php");

  $b_name="";
$b_address="";
$b_email="";
$b_mobile="";
$b_website="";
$r_name="";
if(isset($_GET['lead'])){
	$lead=$obj->display3('select l.*,f.*,b.name b_name,b.address b_address,b.email b_email,
		b.mobile b_mobile,b.website b_website,r.name r_name,
		a.bank_name,a.bank_address,a.bank_beneficiary,a.account_no,a.iban,cp.name country,dc.id agreementNo
		from dm_lead l left join dm_fee f on f.service=l.service_interest 
		and f.country=l.country_interest left join dm_branch b on b.id=l.branch 
		left join dm_region r on r.id=l.region left join dm_accounts a on b.id=a.branch_id join dm_country_proces cp on cp.id=l.country_interest left join dm_lead_contract dc on dc.leadid=l.id where l.id='.$_GET['lead']);

  $lead1=$lead->fetch_array();
  $coun=$obj->display('dm_employee','id='.$lead1['Counsilor']);
  $coun1=$coun->fetch_array();
  $prog=$obj->display('dm_service','id='.$lead1['service_interest']);
  $prog1=$prog->fetch_assoc();
  $country=ucwords(strtolower($lead1['country']));
  $b_name=ucwords(strtolower($lead1['b_name']));
  $b_av=($lead1['branch']=="4"?"DIS":"DM");
$b_address=$lead1['b_address'];
$b_email=$lead1['b_email'];
$b_mobile=$lead1['b_mobile'];
$b_website=$lead1['b_website'];
$r_name=$lead1['r_name'];
}

$obs = $obj->display('obs_sheet','leadid='.$_GET['lead']);
if($obs->num_rows > 0) { $obs1 = $obs->fetch_array(); }
  ?>
<style type="text/css">
	span.cls_003{font-family:"Calibri Bold",serif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri Bold",serif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none;padding: 10px 0px;}
span.cls_014{font-family:"Calibri Bold",serif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_014{font-family:"Calibri Bold",serif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none;padding: 10px 0px;}
span.cls_002{font-family:"Calibri",serif;font-size: 12.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size: 12.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}

.fees-table {
  border-collapse: collapse;
  width: 100%;
      font-family: "Calibri Bold",serif;
    font-size:  12.5px;
}

.fees-table th, td {
  text-align: left;
  padding: 3px;
}

.fees-table tr:nth-child(even){background-color: #f2f2f2;}


.fees-table tr th {
  background-color: #4CAF50;
  color: white;
  border: 1px solid black;
}

.front-page-table{
  border-collapse: collapse;
  width: 60%;
  font-family: "Calibri Bold",serif;
  font-size:  12.5px;
  font-weight:bold;
  margin-top: 25px;
}

.page{
	padding: 25px 
}
ol{    font-family: "Calibri",serif;
    font-size: 12.5px;
    color: rgb(0,0,0);
    font-weight: normal;
    font-style: normal;
    text-decoration: none;
    padding-left: 50px;
}
ol>li{
	list-style-type:   lower-alpha;
}
ol>li ul>li,.cls_002 ul>li{
	list-style-type:   disc;
}
.cls_002 ul{
  padding-left: 38px;
}
ol li ol li{
	list-style-type:   lower-roman;
}
* {transition: none !important}

</style>

<div class="col-sm-12">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<!-- <h4 class="mb-3" style="color:#2cb674;"></h4> -->
			</div>
            <div class="col-sm-6" align="right">
            <form action="process/pdf process.php" target="_blank" id="text_form" method="POST">
            <input type="hidden" name="html_text">
            <input type="hidden" name="b_name" value="<?php echo $b_name?>">
            <input type="hidden" name="b_address" value="<?php echo  htmlentities($b_address);?>">
            <input type="hidden" name="b_mobile" value="<?php echo $b_mobile?>">
            <input type="hidden" name="b_email" value="<?php echo $b_email?>">
            <input type="hidden" name="obsid" value="<?php echo $_GET['lead'];?>">
            <button type="submit" class="btn btn-info">Print</button>
            </form>
            </div>
        </div>

            <div class="row">
            <div class="col-sm-8" id="" style="margin-left: 16%;border: 3px solid black;padding-left: 60px;padding-right: 60px;text-align: justify;">
            <div id="agreement-content">
            <div  class="cls_003" align="center"><span class="cls_003">Student Visa Observation sheet</span></div>
            <div align="center"><span style="font-family:Calibri Boldserif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none">Counsellor’s observation / Discussion with the client</span></div>
            <div align="center"><span class="cls_003">AG--</span></div>
            <div  class="cls_002" style="padding-top:20px"><span class="cls_002">1). The applicant should provide CV, IELTS, Passport Copy, Education documents for the most recent study immediately with the college, location and course with budget preferences.</span></div>
            <div  class="cls_002"><span class="cls_002">2). The applicant must provide employment proof, if working.</span></div>
            <div  class="cls_002"><span class="cls_002">3). The applicant must have valid IELTS (Academics), TOEFL, GRE, GMAT, PTE (Requirement depends on the institute)</span></div>
            <div  class="cls_002"><span class="cls_002">4) The applicant must declare about the previous refusals and relatives in respective country.</div></span>
            <div  class="cls_002"><span class="cls_002">5). The VFS will charge separately for processing applications apart from the visa application fee.</span></div>
            <div  class="cls_002"><span class="cls_002">6). The processing time of application with respective authorities begins from the day of application submission to the visa office not from the starting day with DM Consultants.</span></div>
            <div  class="cls_002"><span class="cls_002">7). The visa stamping is not guaranteed as it is completely the decision of the visa officer.</span><div>
            <div  class="cls_002"><span class="cls_002">8). The applicant will have to wait for the decision until the processing is completed by the respective visa office.</span></div>
            <br/>
            <div><span style="font-family:Calibri Boldserif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none">Note: The client voluntarily withdraws the immigration case at any stage, no refunds will be given.</span></div>
            <pagebreak/>

            <table class="fees-table">
            <tr><td style="border: 1px solid black;">Attributes</td><td style="border: 1px solid black;">Details</td></tr>
            <tr><td style="border: 1px solid black;">DOB & Nationality</td><td style="border: 1px solid black;"><?= $obs1['adets']; ?></td><tr>
            <tr><td style="border: 1px solid black;">Education</td><td style="border: 1px solid black;"><?= $obs1['edets']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Work Experience</td><td style="border: 1px solid black;"><?= $obs1['wdets']; ?></td></tr>
            <tr><td style="border: 1px solid black;">GAP</td><td style="border: 1px solid black;"><?= $obs1['gap']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Funds Available & Source of Funds</td><td style="border: 1px solid black;"><?= $obs1['funds']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Country of Interest</td><td style="border: 1px solid black;"><?= $obs1['coi']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Tution Fees</td><td style="border: 1px solid black;"><?= $obs1['fee']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Living Expense</td><td style="border: 1px solid black;"><?= $obs1['lexp']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Funds to shown as per destination Country</td><td style="border: 1px solid black;"><?= $obs1['pofc']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Primary Applicant</td><td style="border: 1px solid black;"><?= $obs1['paf']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Spouse</td><td style="border: 1px solid black;"><?= $obs1['spf']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Each Kid</td><td style="border: 1px solid black;"><?= $obs1['kpf']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Application fee (Respective Govt.)</td><td style="border: 1px solid black;"><?= $obs1['appfee']; ?></td></tr>
            <tr><td style="border: 1px solid black;">Insurance Charges (If applicable)</td><td style="border: 1px solid black;"><?= $obs1['Inschg']; ?></td></tr>
            <tr><td style="border: 1px solid black;">VFS Charges (In local Currency)</td><td style="border: 1px solid black;"><?= $obs1['vfs']; ?></td></tr>
            </table>
            <div><span style="font-family:Calibri Boldserif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none">Remarks (If Any):<br/><?= $obs1['rem']; ?></span></div>
          <br/>
            <!-- <pagebreak/> -->
            <table width="100%" style="margin-top: 60px">
            <tr><td>Client Name:&nbsp;<?php echo $lead1['fname'].$lead1['mname'].$lead1['lname'];?></td><td style="text-align:right">Counsellor:<?php echo $coun1['name'];?></td></tr>
            </table>
            <br/>
            <table class="fees-table">
            <tr><td>Client Signature</td><td></td></tr>
            <tr><td>DM Consultants</td><td></td></tr>
            <tr><td>Date</td><td><?php echo date('d-m-Y');?></td></tr>
            </table>
            <?php if($lead1['region']==6 || $lead1['region']==10) { ?>
            <br/>
            <br/>
            <div><span style="font-family:Calibri Boldserif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none">Would you like to participate in our Referral Reward Programme? Earn <?php echo (($lead1['region']==3 || $lead1['region']==4 || $lead1['region']==5)?"AED 100.00":"");?> <?php echo ($lead1['region']==7?"QAR 100.00":"");?><?php echo ($lead1['region']==8?"OMR 10.00":"");?><?php echo ($lead1['region']==9?"KWD 10.00":"");?> on each referral as a bonus.</span></div>
            <div><span style="font-family:Calibri Boldserif;font-size: 12.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none">*Please ask your Immigration Counsellor to understand the terms and conditions.</span></div>
            <?php } ?>

            </div>
            </div>
            </div>




            
    </div>
</div>

<script>
data={};
data.html_text=document.getElementById('agreement-content').innerHTML;
$.ajax({
    url: "<?php echo $base_url;?>/process/pdf process.php",
    type: "POST",
    data: data,
  success:function(result){
      /*tr.fadeOut(1000, function(){
        $(this).remove();
      });
      if(result=="200"||result=="200200"){
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#success-alert").slideUp(500);
        });
      }
      
      $(".last-td-"+user_id).find(".loader").css("display","none");
      $(".last-td-"+user_id).find("button").css("display","");*/
      console.log(result);

    },
  error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus); 
        console.log("Error: " + errorThrown); 
    } 
  });
$('#text_form').submit(function() {
  $("[name='html_text']").val($("#agreement-content").html());
});
</script>
