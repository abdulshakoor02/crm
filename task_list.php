<?php
include_once("header.php");	
?>
<div class="col-sm-10">
	<h4 class="mb-3" style="color:#2cb674;">Task Management <a href="javascript:void(0);" class="btn btn-info pull-right" data-toggle="modal" data-target="#newForm"  style="background:#2cb674;">Add New <i class="fa fa-plus"></i></a></h4>
	<form name="search" action="" method="post">

<div class="row">

<div class="col-sm-2 form-group">

<label >Start Date</label>

<input type="text" class="form-control" id="sdate" name="sdate" value="<?php if($_POST['sdate']) echo $_POST['sdate']; else  echo date('d-m-Y')?>" >

</div>

<div class="col-sm-2 form-group">

<label >End Date</label>

<input type="text" class="form-control" id="edate" name="edate" value="<?php if($_POST['edate']) echo $_POST['edate']; else echo date('d-m-Y')?>" >

</div>

<div class="col-sm-2 form-group">
<label >Status</label>
<select class="form-control" name="stat"  id="stat">
<option value="">Select</option>
<option value="pending" <?php if($_POST['stat']=='pending') { echo 'selected="selected"';}?> >pending</option>
<option value="returned" <?php if($_POST['stat']=='returned') { echo 'selected="selected"';}?> >Returned</option>
<option value="Completed" <?php if($_POST['stat']=='Completed') { echo 'selected="selected"';}?> >Completed</option>
				 </select>
</div>

<div class="col-sm-2 form-group"><label>&nbsp;</label><br /><input type="submit" class="btn btn-info" name="search" value="Search" ></div>
</div>

			
			<table class="table table-striped table-bordered" id="dataTables-Table" style="width:100%">
				<thead>
					<tr>
					  <th>Task</th>
					  <th>Date</th>
					  <th>Assign To</th>
					  <th>Assign By</th>
					  <th>Status</th>
					  <th>Date Completed</th>
					  <th style="text-align:right">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$i=1;
				if($_POST['search']){
				$query.=" and dob between '".date('Y-m-d',strtotime($_POST["sdate"]))."' and '".date('Y-m-d',strtotime($_POST["edate"]))."'";
				}
				if($_POST['stat']!=""){
				$query.=" and status='".$_POST['stat']."'";
				}
				$re=$obj->display("dm_task","(asignBy=".$_SESSION['ID']." or asignTo=".$_SESSION['ID'].")".$query); 	
				while($res2=$re->fetch_array())
				{ 
				$r=$obj->display("dm_employee","id=".$res2['asignTo']); $r2=$r->fetch_array();
				$aby=$obj->display("dm_employee","id=".$res2['asignBy']); $aby1=$aby->fetch_array();
				?>  
				<tr id="item-<?=$res2['id']?>" <?php if ($res2['notf']==1) { echo "style=background:#69ea8b;";} ?>>
				 <td><?=$res2['task'];?></td>
				 <td><?=date('d-m-Y',strtotime($res2['dob']));?></td>
				 <td><?=$r2['name'];?></td>
				 <td><?=$aby1['name'];?></td>
				 <td><?php if ($res2['status']=="Completed"){ echo"Completed"; } else {?><select name="statusTo" style="font-size:14px"  id="statusTo" onchange="changeStatus(this.value, <?php echo $res2['id'];?>)">
<option value="">Select</option>
<option value="pending" <?php if($res2['status']=='pending') { echo 'selected="selected"';}?> >pending</option>
<option value="returned" <?php if($res2['status']=='returned') { echo 'selected="selected"';}?> >Returned</option>
<option value="Completed" <?php if($res2['status']=='Completed') { echo 'selected="selected"';}?> >Completed</option>
				 </select><?php } ?></td>
				 <td><?=$res2['doc'];?></td>
				 <td style="text-align:right" >
				 <?php
				//   if(strtotime($res2['dob']) >= strtotime(date('Y-m-d'))) {
					  ?>
					 <a href="javascript:void(0);" data-toggle="modal" data-target="#editForm<?=$res2['id']?>" class="btnEditAction"><i class="fa fa-edit" title="EDIT"></i></a>
					 <?php if ($res2['notf']==1) { ?>
					 <a href="javascript:void(0);" onclick="read(<?=$res2['id']?>)">mark read</i></a>
					 <?php } ?>
					 <?php 
					 if($res2['asignBy']==$_SESSION['ID']) {
						 if($res2['status']!="Completed"){
						 ?>
					 <a href="javascript:void(0);" id="<?=$res2['id'];?>" class="btnDeleteAction"><i class="fa fa-trash" title="DELETE"></i></a> 
					 <?php 
					//  }
						 }}
					  ?>
				 </td>   
				</tr>

<div class="modal fade" id="editForm<?=$res2['id']?>" tabindex="-1" role="dialog" >
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Edit Task</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
</div>

<div class="modal-body">
			<div class="alert alert-success" id="alert-success<?=$res2['id']?>" style="display:none">Data Saved Successfully.</div>
			<div class="alert alert-danger" id="alert-danger<?=$res2['id']?>" style="display:none">Somthing error. Value not saved.</div>
<form id="popForm<?=$res2['id']?>" method="post" class="" action="" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?=$res2['id']?>" />
<input type="hidden" name="action" value="edit" />
<input type="hidden" name="emp" value="<?=$_SESSION['ID'];?>" />
<?php
$rem=$obj->display('task_remarks','taskid='.$res2['id']);
if($rem->num_rows > 0){
	while($rem1=$rem->fetch_array()) {
		echo $rem1['remarks']." added by ".$rem1['emp']." on ".$rem1['date'];
		echo "<br/>";
	}
}
?>
<br/>
	<!-- <div class="row">
		<div class="form-group col-sm-12">
			<label>Task</label>
			<input type="text" class="form-control" name="task" value="<?=$res2['task']?>" />
		</div>
		<div class="form-group col-sm-6">
			<label>Date</label>
			<input type="text" class="form-control" name="dob" id="dob<?=$res2['id']?>" value="<?=date('d-m-Y',strtotime($res2['dob']))?>" />
		</div>
		<div class="form-group col-sm-6">
			<label>Assign To</label>
			<select type="text" class="form-control" name="asignTo" >
			<?php
			// $reg=$obj->display("dm_employee","status=1 and id!=1 and id!=".$_SESSION['ID']." order by name ASC"); 	
			// while($reg2=$reg->fetch_array())
			// { 
			?>  
			<option value="<?php echo $reg2['id'];?>" <?php if($reg2['id']==$res2['role']) { echo 'selected="selected"';}?> ><?php echo $reg2['name'];?></option>
			<?php
		//  }
		  ?>
			</select>
		</div>
	</div> -->
	<div class="row">
	<label>Remarks</label>
	<div class="form-group col-8">
	<textarea class="form-control" rows=4 id="remarks" name="remarks"></textarea>
	</div>
	</div>
<div class="row">
	   <div class="form-group col-12">
			<button type="submit" class="btn btn-primary" >SAVE</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		</div>
</div>
</form>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function() {
	$('#dob<?=$res2['id']?>').datepicker({    format: 'dd-mm-yyyy',	autoclose: true});

$('#popForm<?=$res2['id']?>').formValidation({
framework: 'bootstrap',
excluded: ':disabled',
icon: {
valid: 'fa fa-ok',
invalid: 'fa fa-remove',
validating: 'fa fa-refresh'
},
// fields: {
//           task: { validators: { notEmpty: { message: 'Task is required' }}},
//             dob: { validators: { notEmpty: { message: 'Date is required' }}},
//             asignTo: { validators: { notEmpty: { message: 'Assign is required' }}}
// }
})
.on('success.form.fv', function(e) {
 var formData = new FormData($('#popForm<?=$res2['id']?>')[0]);
           $.ajax({
               url: 'process/task_process.php',
                type: 'POST',
				enctype: 'multipart/form-data',
				dataType: 'json',
               data: formData,
			   processData: false,
            	contentType: false,
           		 cache: false,
success: function(result) { 
	if(result.status=='success')
	{
	$('#alert-success<?=$res2['id']?>').css('display','block');
	setTimeout(function(){ $('#alert-success<?=$res2['id'];?>').css('display','none');},2000);
	setTimeout(function(){ location.reload();},2000);
	}
	else
	{
	$('#alert-danger<?=$res2['id']?>').css('display','block');
	setTimeout(function(){ $('#alert-danger<?=$res2['id'];?>').css('display','none');},3000);
	}
}
});
});
});
</script>											
<?php $i++;} ?>

				</tbody>

			</table>

			<!-- /.table-responsive -->

</div>
                <!-- /.col-lg-12 -->
       
<div class="modal" id="newForm">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">New Task</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            </div>
            <div class="modal-body">
							<div class="alert alert-success" style="display:none">Data Saved Successfully.</div>
							<div class="alert alert-danger" style="display:none">Somthing error. Value not saved.</div>
                <form id="popForm" method="post" class="" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="add" />
				<div class="row">
                    <div class="form-group col-sm-12">
                       <label>Task</label>
                            <input type="text" class="form-control" name="task" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Date</label>
                            <input type="text" class="form-control" name="dob" id="dob" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Assign to</label>
                            <select type="text" class="form-control" name="asignTo" >
							<option value="">Select</option>
							<?php
								$reg=$obj->display("dm_employee","status=1 and id!=1 and id!=".$_SESSION['ID']." order by name ASC"); 	
								while($reg2=$reg->fetch_array())
								{ 
							?>  
								<option value="<?php echo $reg2['id'];?>"><?php echo $reg2['name'];?></option>
								<?php } ?>
						</select>
                    </div>
                   
					</div>
				<div class="row">
                        <div class="form-group col-12">
                            <button type="submit" class="btn btn-primary" >SAVE</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
               </div>
			    </form>
           </div>
        </div>
    </div>
</div>

<?php 	include_once("footer.php");	?>

	<script src="js/formvalidation.js"></script>
<script>
$(document).ready(function() {
	$('#sdate').datepicker({    format: 'dd-mm-yyyy',	autoclose: true});
	$('#edate').datepicker({    format: 'dd-mm-yyyy',	autoclose: true}); 
	$('#dob').datepicker({    format: 'dd-mm-yyyy',	autoclose: true}); 
	$('#popForm').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'fa fa-ok',
            invalid: 'fa fa-remove',
            validating: 'fa fa-refresh'
        },
        fields: {
           task: { validators: { notEmpty: { message: 'Task is required' }}},
            dob: { validators: { notEmpty: { message: 'Date is required' }}},
            asignTo: { validators: { notEmpty: { message: 'Assign is required' }}}
        }
    })
	.on('success.form.fv', function(e) {
           e.preventDefault();
		   var formData = new FormData($('#popForm')[0]);
            $.ajax({
                url: 'process/task_process.php',
                type: 'POST',
				enctype: 'multipart/form-data',
				dataType: 'json',
               data: formData,
			   processData: false,
            	contentType: false,
           		 cache: false,
                success: function(result) {
					if(result.status=='success')
					{
					$('.alert-success').css('display','block');
					setTimeout(function(){ $('.alert-success').css('display','none');},1000);
					setTimeout(function(){ location.reload();},1000);
					}
					else
					{
					$('.alert-danger').css('display','block');
				setTimeout(function(){ $('.alert-danger').css('display','none');},1000);
					}
               }
           });
        });

	$('body').on('click','.btnDeleteAction',function(){ 
				 var tr = $(this).closest('tr');
					del_id = $(this).attr('id');
		if(confirm("Want to delete this?"))
		{ 
					$.ajax({
						url: "<?php echo $base_url;?>/process/task_process.php",
						type: "POST",
						cache: false,
						data:'&del_id='+del_id+'&action=delete',
					success:function(result){
							tr.fadeOut(1000, function(){
								$(this).remove();
							});
						}
					});
		}
	});
});

function changeStatus(asign,lead){
if(asign!="")
{	$.ajax({
				url: "<?php echo $base_url;?>/process/task_stat_process.php",
				type: "POST",
				cache: false,
				dataType: 'json',
				data:'&asign='+asign+'&lead='+lead,
				success:function(result){
				alert("Status Changed");
				location.reload();
				}
			});
}
}

function read(n){
	$.ajax({
				url: "<?php echo $base_url;?>/process/notf_process.php",
				type: "POST",
				cache: false,
				dataType: 'json',
				data:'&id='+n,
				success:function(result){
				alert("Status Changed");
				location.reload();
				}
			});	
}
</script>			