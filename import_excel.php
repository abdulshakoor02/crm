<?php include_once ('header.php');
require './vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

$duplicateRecord = [];
$skipRecord = [];
if (isset($_POST["import"])) {

	$allowedFileType = [
		'application/vnd.ms-excel',
		'text/xls',
		'text/xlsx',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	];

	if (in_array($_FILES["file"]["type"], $allowedFileType)) {

		$targetPath = 'uploads/' . $_FILES['file']['name'];
		move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

		$Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

		$spreadSheet = $Reader->load($targetPath);
		$excelSheet = $spreadSheet->getActiveSheet();
		$spreadSheetAry = $excelSheet->toArray();

		$sheetCount = count($spreadSheetAry);

		for ($i = 1; $i < $sheetCount; $i++) {

			$fname = isset($spreadSheetAry[$i][0]) ? $spreadSheetAry[$i][0] : '';
			$mname = isset($spreadSheetAry[$i][1]) ? $spreadSheetAry[$i][1] : '';
			$lname = isset($spreadSheetAry[$i][2]) ? $spreadSheetAry[$i][2] : '';
			$email = isset($spreadSheetAry[$i][3]) ? $spreadSheetAry[$i][3] : '';
			$mobile = isset($spreadSheetAry[$i][4]) ? $spreadSheetAry[$i][4] : '';
			$alternate_num = isset($spreadSheetAry[$i][5]) ? $spreadSheetAry[$i][5] : '';
			$nationality = isset($spreadSheetAry[$i][6]) ? $spreadSheetAry[$i][6] : '';
			$address = isset($spreadSheetAry[$i][7]) ? $spreadSheetAry[$i][7] : '';
			$dob = isset($spreadSheetAry[$i][8]) ? $spreadSheetAry[$i][8] : '';
			$gender = isset($spreadSheetAry[$i][9]) ? $spreadSheetAry[$i][9] : '';
			$country_interest = isset($spreadSheetAry[$i][10]) ? $spreadSheetAry[$i][10] : '';
			$service_interest = isset($spreadSheetAry[$i][11]) ? $spreadSheetAry[$i][11] : '';
			$relative = isset($spreadSheetAry[$i][12]) ? $spreadSheetAry[$i][12] : '';
			$market_source = isset($spreadSheetAry[$i][13]) ? $spreadSheetAry[$i][13] : '';
			$mstatus = isset($spreadSheetAry[$i][14]) ? $spreadSheetAry[$i][14] : '';
			$fnames = isset($spreadSheetAry[$i][15]) ? $spreadSheetAry[$i][15] : '';
			$emails = isset($spreadSheetAry[$i][16]) ? $spreadSheetAry[$i][16] : '';
			$phones = isset($spreadSheetAry[$i][17]) ? $spreadSheetAry[$i][17] : '';
			$mobiles = isset($spreadSheetAry[$i][18]) ? $spreadSheetAry[$i][18] : '';
			$sedu = isset($spreadSheetAry[$i][19]) ? $spreadSheetAry[$i][19] : '';
			$kids = isset($spreadSheetAry[$i][20]) ? $spreadSheetAry[$i][20] : '';
			$sexp = isset($spreadSheetAry[$i][21]) ? $spreadSheetAry[$i][21] : '';
			// $mdate = isset($spreadSheetAry[$i][21]) ? $spreadSheetAry[$i][21] : '';
			// $time = isset($spreadSheetAry[$i][22]) ? $spreadSheetAry[$i][22] : '';
			// $mtype = isset($spreadSheetAry[$i][23]) ? $spreadSheetAry[$i][23] : '';
			$lead_category = isset($spreadSheetAry[$i][22]) ? $spreadSheetAry[$i][22] : '';
			$enquiry = isset($spreadSheetAry[$i][23]) ? $spreadSheetAry[$i][23] : '';


			if ($email === '' || $mobile === '') {
				array_push($skipRecord, $i);
				$skip_error = "skip-error";
				$message_skip = "Record Skip";
				continue;
			}

			$ext = $obj->display('dm_lead', 'email="' . $email . '" or mobile="' . $mobile . '"');

			if ($ext->num_rows == 0) {
			
				if ($dob != "") {
					$dob = date('Y-m-d', strtotime($dob));
				}

				$data = array(
					'fname'  =>  $fname,
					'mname'  =>  $mname,
					'lname'  =>  $lname,
					'email'  =>  $email,
					'phone'  =>  $alternate_num,
					'mobile'  =>  $mobile,
					'nationality'  =>  $nationality,
					'address'  =>  $address,
					'dob'  =>  $dob,
					'gender'  =>  $gender,
					'country_interest'  =>  $country_interest,
					'service_interest'  =>  $service_interest,
					// 'relative' => $relative,
					'market_source'  =>  $_POST['market_source'],
					// 'mstatus' => $mstatus,
					//spouse data
					// 'fnames' => $fnames,
					// 'emails' => $emails,
					// 'phones' => $phones,
					// 'mobiles' => $mobiles,
					// 'sedu' => $sedu,
					// 'kids' => $kids,
					// 'sexp' => $sexp,
					//end
					// 'lead_category' => $lead_category,
					// 'enquiry'  =>  $enquiry,
					'assignTo'  =>  $_SESSION["ID"],
					'Counsilor'  =>  $_SESSION["ID"],
					// 'appointment'  =>  $appointment, no entry on add new page
					'regdate'  =>  date('Y-m-d'),
					'last_updated' => date('d-m-Y h-i-sa'),
					// 'followup'  =>  date('Y-m-d', strtotime($_POST['followup'])), no entry on add new page
					// 'convet'  =>  $_POST['convet'], no entry on add new page
					// 'type'  =>  $_POST['type'],no entry on add new page
					'branch'  =>  $_SESSION["BRANCH"],
					'region'  =>  $_SESSION["REGION"]
				);
				$odr = $obj->insert('dm_lead', $data);

				if (!empty($odr)) {
					$type = "success";
					$message = "Excel Data Imported into the Database";
				} else {
					$type = "error";
					$message = "Problem in Importing Excel Data";
				}
			} else {
				array_push($duplicateRecord, $i);
				$duplicate_error = "duplicate-error";
				$message_dup = "Duplicate error";
			}
		}
	} else {
		$type = "error";
		$message = "Invalid File Type. Upload Excel File.";
	}
}

?>

<style>
	#response {
		padding: 5px;
		margin-top: 0px;
		border-radius: 2px;
		display: none;
	}

    .success {
		background: #c7efd9;
		border: #bbe2cd 1px solid;
	}

	.duplicate-error,
	.skip-error,
	.error {
		background: #fbcfcf;
		border: #f3c6c7 1px solid;
	}

	div#response.display-block {
		display: block;
	}

    </style>

<div class="col-sm-10">

<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div id="response" class="<?php if (!empty($type)) {
													echo $type . " display-block";
												} ?>"><?php if (!empty($message)) {
															echo $message;
														} ?></div>
					<div id="response" class="<?php if (!empty($duplicate_error)) {
													echo $duplicate_error . " display-block";
												} ?>"><?php if (!empty($duplicateRecord)) {
															// echo $message_dup;
															echo "Total Duplicate Entry Count " . count($duplicateRecord);
															echo "<br>Duplicate Entry Record Number ";
															foreach ($duplicateRecord as $key => $value) {
																echo $value + 1 . "&nbsp;&nbsp;";
															}
														} ?></div>
					<div id="response" class="<?php if (!empty($skip_error)) {
													echo $skip_error . " display-block";
												} ?>"><?php if (!empty($message_skip)) {
															// echo $message_skip;
															echo "Total Skipped Count " . count($skipRecord);
															echo "<br>Skipped Entry Record Number ";
															foreach ($skipRecord as $key => $value) {
																echo $value + 1 . "&nbsp;&nbsp;";
															}
														} ?></div>
				</div>
			</div>


<form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
							<div class="col-sm-12">
							<div class="row">
							<div class="col-sm-3 form-group">
							<label >Select file</label>
								<input type="file" name="file" id="file" accept=".xls,.xlsx">
								</div>
								<div class="col-sm-2 form-group"><label >Marketing Source</label>
<select class="form-control" name="market_source" required>
	<option value="">Select</option>
	<?php $sou=$obj->display('dm_source','status=1 and id in (7,8,16,17) order by name');
	while($sou1=$sou->fetch_array())
	{
	?>
	<option value="<?php echo $sou1['id'];?>"><?php echo $sou1['name'];?></option>
	<?php } ?>
	
	</select>
	
	</div>
	</div>
								<button type="submit" id="submit" name="import" class="btn btn-info">Import as Excel</button>
							</div>
							
						</form>

</div>

<?php include_once ('footer.php'); ?>