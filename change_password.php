<?php include_once("header.php");	
if($_POST['oldpassword'])
{
$old=$obj->display('dm_employee','id='.$_SESSION['ID']);
    $old1 = $old->fetch_array();
if($old1['password']!=$_POST['oldpassword'])
{
    header("location:change_password.php?match=no");
}
if($old1['password']==$_POST['oldpassword'])
{
	$data = array(
    			"password" => $_POST['newpassword']
				);
    $obj->update('dm_employee',$data,'id='.$_SESSION['ID']);
    header("location:change_password.php?match=yes");
}
}
if($_REQUEST['match']=="yes"){
    // echo "ahkahskdh";die;
  echo "<script type='text/javascript'>Swal.fire('Password Updated','Your password was updated succesfully')</script>";
}
?>
<!-- <script>
alert("Password Changed Successfully");
window.location.href="change_password.php";
</script> -->
<?php


?>
<style>
ul, li {
    margin:0;
    padding:0;
    list-style-type:none;
}

#pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}

/* #pswd_info {
    position:absolute;
    bottom:-75px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
} */

#pswd_info {
    display:none;
}

.invalid {
    /* background:url(../images/invalid.png) no-repeat 0 50%; */
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /* background:url(../images/valid.png) no-repeat 0 50%; */
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
</style>

		<div class="col-sm-10">
		<div class="row"><div class="col-sm-6"><h4 class="mb-3" style="color:#2cb674;">Change Password</h4></div></div>
<?php if ($_GET['match']=='no'){ ?>
<h4 style="color:red">Old Password does not match</h4>
<?php } ?>
<br/>
<form name="search" action="" method="post">
<div class="row">
<div class="col-sm-12 form-group">
<label >Current Password</label>
<input type="text" class="form-control" name="oldpassword" required>
</div>
<div class="col-sm-12 form-group">
<label>New Password</label>
<input type="text" class="form-control" id="newpassword" name="newpassword" >
</div>


			
<div class="col-sm-3 form-group"><label >&nbsp;</label><br /><input type="submit" class="btn btn-info" onclick="confirmation(event,this.form)" name="search" value="Update" ></div>
</div>
</form>

<div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>

<hr />

		</div>
<?php include_once("footer.php"); ?>

<script src="js/formvalidation.js"></script>
<script>
$(document).ready(function() {
    $('#popForm').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'fa fa-ok',
            invalid: 'fa fa-remove',
            validating: 'fa fa-refresh'
        },
        fields: {
            oldpassword: { validators: { notEmpty: { message: 'Current Password required' }}},
            newpassword: { validators: { notEmpty: { message: 'New Password required' }}}
        }
    })
	.on('success.form.fv', function(e) {
		e.preventDefault();
		$('#popForm').formValidation('defaultSubmit');
    });
    
$('#newpassword').keyup(function(){
    // console.log('yea');
    var pswd = $('#newpassword').val();
    var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if (pswd.match(decimal)) {
        // console.log('no');
        $('#pswd_info').hide();
} else {
    $('#pswd_info').show();
}

// if ( pswd.match(/[A-z]/) ) {
//     $('#pswd_info').show();
// } else {
//     $('#pswd_info').hide();
// }

// //validate capital letter
// if ( pswd.match(/[A-Z]/) ) {
//     $('#pswd_info').show();
// } else {
//     $('#pswd_info').hide();
// }

// //validate number
// if ( pswd.match(/\d/) ) {
//     $('#pswd_info').show();
// } else {
//     $('#pswd_info').hide();
// }

});
});

function confirmation(ev,f) {
      ev.preventDefault();
      // var d = $('#datepicker').val();
    //   url = ev.currentTarget.getAttribute('href');

    var pswd = $('#newpassword').val();
    var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if (pswd.match(decimal)) {
        // console.log('no');
        $('#pswd_info').hide();
        f.submit();
} else {
    $('#pswd_info').show();
}
    }


</script>			