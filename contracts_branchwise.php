<?php include_once("header.php");

?>
<div class="col-sm-10">
	<h4 class="mb-3" style="color:#2cb674;">Report of Number of Contracts per Branch</h4>
	<div id="alert_message"></div>
    <form name="search" action="" method="post">

    <div class="row">

<div class="col-sm-2 form-group">

<label >Start Date</label>

<input type="text" class="form-control" id="sdate" name="sdate" value="<?php if($_POST['sdate']) echo $_POST['sdate']; else  echo date('d-m-Y')?>" >

</div>

<div class="col-sm-2 form-group">

<label >End Date</label>

<input type="text" class="form-control" id="edate" name="edate" value="<?php if($_POST['edate']) echo $_POST['edate']; else echo date('d-m-Y')?>" >

</div>

<div class="col-sm-2 form-group"><label>&nbsp;</label><br /><input type="submit" class="btn btn-info" name="search" value="Search" ></div>

</div>

</form>

	<hr />
    <?php  if($_POST) { ?>
	<table class="table table-striped table-bordered" id="myTable" style="width:100%">

				<thead>

					<tr>
						<th>Sr no.</th>
						<th>Branch</th>
						<th>Canada</th>
                        <th>Australia</th>
                        <th>Visit</th>
                        <th>Student</th>
                        <th>Total</th>
						</tr>
						</thead>
						<tbody>
							<?php
							$result = $obj->display3("SELECT COUNT(*) as total, (SELECT name from dm_region WHERE id=region) as branch,region FROM `dm_lead` WHERE feeAgreeDate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 GROUP by region");
							// print_r($result);die;
							if($result->num_rows>0)
							{
								$i=1;
								while($row=$result->fetch_assoc())
								{
                                    $can=$obj->display3("SELECT COUNT(*) as can FROM `dm_lead` WHERE feeAgreeDate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Skill' and country_interest=2 and region=".$row['region']);$can1=$can->fetch_assoc();
                                    $aus=$obj->display3("SELECT COUNT(*) as aus FROM `dm_lead` WHERE feeAgreeDate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Skill' and country_interest=1 and region=".$row['region']);$aus1=$aus->fetch_assoc();
                                    $vis=$obj->display3("SELECT COUNT(*) as vis FROM `dm_lead` WHERE feeAgreeDate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Visit' and region=".$row['region']);$vis1=$vis->fetch_assoc();
                                    $stud=$obj->display3("SELECT COUNT(*) as stud FROM `dm_lead` WHERE feeAgreeDate BETWEEN '".date('Y-m-d',strtotime($_POST["sdate"]))."' AND '".date('Y-m-d',strtotime($_POST["edate"]))."' AND paidYet!=0 and type='Student' and region=".$row['region']);$stud1=$stud->fetch_assoc();
									?>
									<tr>
										<td><?=$i;?></td>
                                        <td><?=$row['branch'];?></td>
										<td><?=$can1['can'];?></td>
										<td><?=$aus1['aus'];?></td>
                                        <td><?=$vis1['vis'];?></td>
                                        <td><?=$stud1['stud'];?></td>
                                        <td><?=$row['total'];?></td>
									</tr>
									<?php
									$i++;
								}
							}
							?>
						</tbody>	
						</table>
                        <?php } ?>
						</div>
						<?php include_once('footer.php');?>
						<script>
                        $(function(){
$('#sdate').datepicker({    format: 'dd-mm-yyyy',	autoclose: true}); 
$('#edate').datepicker({    format: 'dd-mm-yyyy',	autoclose: true}); 
});
							$(document).ready(function(){
								$('#myTable').DataTable({
									responsive:true
								});
								});
						</script>			